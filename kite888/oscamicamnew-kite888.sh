#!/bin/sh

#config
pack=oscamicamnew
version=11868
url="https://gitlab.com/hmeng80/softcam/-/raw/main/kite888/"
ipk="$pack-$version.ipk"
install="opkg install --force-reinstall"
status='/var/lib/opkg/status'
package='enigma2-plugin-softcams-oscamicamnew'

if grep -q $package $status; then
echo "> removing package please wait..."
sleep 3s
opkg remove $package

else

# Remove unnecessary files and folders
[ -d "/CONTROL" ] && rm -r /CONTROL >/dev/null 2>&1
rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1

cleanup() {
    echo "> Performing cleanup..."
    [ -d "/CONTROL" ] && rm -rf /CONTROL >/dev/null 2>&1
    rm -rf /control /postinst /preinst /prerm /postrm /tmp/*.ipk /tmp/*.tar.gz >/dev/null 2>&1
    echo "> Cleanup completed."
}
cleanup

# Download and install plugin
echo "> Downloading "$pack"-"$version" please wait..."
sleep 3s

cd /tmp
set -e
wget --show-progress -q "$url/$ipk"
$install $ipk
set +e
cd ..
wait
rm -f /tmp/$ipk

if [ $? -eq 0 ]; then
echo "> "$pack"-"$version" installed successfully"
sleep 3s
else
echo " installation failed"
fi

fi