#!/bin/sh
if [ -d /usr/lib/enigma2/python/Plugins/Extensions/NovacamPro ]; then
echo "> removing package please wait..."
sleep 3s 
rm -rf /usr/lib/enigma2/python/Plugins/Extensions/NovacamPro > /dev/null 2>&1

status='/var/lib/opkg/status'
package='enigma2-plugin-extensions-novacampro'

if grep -q $package $status; then
opkg remove $package
fi

echo "*******************************************"
echo "*             Removed Finished            *"
echo "*            Uploaded By Haitham          *"
echo "*******************************************"
sleep 3s

else

#remove unnecessary files and folders
if [  -d "/CONTROL" ]; then
rm -r  /CONTROL >/dev/null 2>&1
fi
rm -rf /control >/dev/null 2>&1
rm -rf /postinst >/dev/null 2>&1
rm -rf /preinst >/dev/null 2>&1
rm -rf /prerm >/dev/null 2>&1
rm -rf /postrm >/dev/null 2>&1
rm -rf /tmp/*.ipk >/dev/null 2>&1
rm -rf /tmp/*.tar.gz >/dev/null 2>&1

# Check python
py=$(python -c"from sys import version_info; print(version_info[0])")

echo "> checking dependencies please wait..."
sleep 1s

if [ "$py" = 3 ]; then

for i in curl libc6 libcurl4 python3-core python3-json python3-requests python3-image python3-cryptography python3-pillow
do
opkg install $i >/dev/null 2>&1
done
else
for i in curl libc6 libcurl4 python-core python-json python-requests python-image python-cryptography python-pillow
do
opkg install $i >/dev/null 2>&1
done
fi

#config
pack=novacampro
version=4.0-r0
install="opkg install --force-reinstall"
#check python version
python=$(python -c "import platform; print(platform.python_version())")

case $python in 
2.7.18)
url='https://gitlab.com/eliesat/extensions/-/raw/main/novalerstore/novacam'
ipk="$pack-py2-$version.ipk"
;;
3.9.7|3.9.9)
url='https://gitlab.com/eliesat/extensions/-/raw/main/novalerstore/novacam'
ipk="$pack-py3.9-$version.ipk"
;;

3.11.0|3.11.1|3.11.2|3.11.3|3.11.5|3.11.5|3.11.6)
url='https://gitlab.com/eliesat/extensions/-/raw/main/novalerstore/novacam'
ipk="$pack-py3.11-$version.ipk"
;;
3.12.0|3.12.1|3.12.2|3.12.3|3.12.4|3.12.5|3.12.6|3.12.7|3.12.8)
url='https://gitlab.com/eliesat/extensions/-/raw/main/novalerstore/novacam'
ipk="$pack-py3.12-$version.ipk"
;;
*)
echo "> your image python version: $python is not supported"
sleep 3
exit 1
;;
esac


# Download and install plugin
echo "> Downloading "$pack"-"$version" please wait..."
sleep 3s

cd /tmp
set -e
wget --show-progress "$url/$ipk"
$install $ipk
set +e
install=$?
cd ..
wait
rm -f /tmp/$ipk

echo ''
if [ $install -eq 0 ]; then
echo "> $plugin-$version package installed successfully"
echo "> Uploaded By Haitham "
sleep 3s

else

echo "> $plugin-$version package installation failed"
sleep 3s
fi

fi
exit
